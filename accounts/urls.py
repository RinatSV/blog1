from django.urls import path, include
from .views import SignUpView, profile

app_name = 'accounts'
urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('profile/<int:user_id>/', profile, name='profile')
]