from django.views.generic import CreateView
from django.urls import reverse_lazy
from .forms import CustomUserCreationForm
from .models import CustomUser
from django.shortcuts import render


class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    template_name = 'accounts/signup.html'
    success_url = reverse_lazy('login')


def profile(request, user_id):
    user = CustomUser.objects.get(id=user_id)
    context = {'user': user}
    return render(request, 'accounts/profile.html', context)
