from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse
from .models import Post


class BlogTests(TestCase):

    def SetUp(self):
        self.user = get_user_model().objects.create_user(
            username='testuser',
            email='test@mail.com',
            password='secret'
        )

        self.post = Post.objects.create(
            title='nice title',
            text='nice text',
            author=self.user,
        )

    def test_srting_representation(self):
        post = Post(title='random title')
        self.assertEqual(str(post), post.title)

    def test_get_absolute_url(self):
        self.assertEqual(self.post.get_absolute_url(), '/post_detail/1/')

    def test_post_content(self):
        self.assertEqual(f'{self.post.title}', 'nice title')
        self.assertEqual(f'{self.post.text}', 'nice text')
        self.assertEqual(f'{self.post.author}', 'testuser')

    def test_post_list_view(self):
        response = self.client.get(reverse('blog:home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'nice text')
        self.assertTemplateUsed(response, 'blog/home.html')

    def test_post_detail_view(self):
        response = self.client.get('/post_detail/1/')
        no_response = self.client.get('/post_detail/10000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'nice title')
        self.assertTemplateUsed(response, 'blog/post_detail.html')

    def test_post_create_view(self):
        response = self.client.post(reverse('blog:post_new'), {
            'title': 'created title',
            'text': 'created text',
            'author': self.user,
        })
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'created title')
        self.assertContains(response, 'created text')

    def test_post_update_view(self):
        response = self.client.post(reverse('blog:post_edit', args='1'), {
            'title': 'updated title',
            'text': 'updates text',
        })
        self.assertEqual(response.status_code, 302)

    def test_post_delete_view(self):
        response = self.client.get(reverse('blog:post_delete', args='1'))
        self.assertEqual(response.status_code, 200)
