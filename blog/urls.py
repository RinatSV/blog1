from django.urls import path
from .views import (
    BlogListView,
    BlogDetailView,
    BlogNewView,
    BlogEditView,
    BlogDeleteView,
)

app_name = 'blog'
urlpatterns = [
    path('', BlogListView.as_view(), name='home'),
    path('post_detail/<int:pk>/', BlogDetailView.as_view(), name='post_detail'),
    path('post/new/', BlogNewView.as_view(), name='post_new'),
    path('post/edit/<int:pk>/', BlogEditView.as_view(), name='post_edit'),
    path('post/delete/<int:pk>/', BlogDeleteView.as_view(), name='post_delete'),
]